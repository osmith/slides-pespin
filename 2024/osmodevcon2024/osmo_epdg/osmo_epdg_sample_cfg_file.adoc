[source,json]
----
[%% ===========================================
 %% Application config
 %% ===========================================
 {osmo_epdg,
  [% GSUP Server connection parameters
   {gsup_local_ip, "127.0.0.1"},
   {gsup_local_port, 4222},
   % Diameter SWx Connection parameters
   {dia_swx_remote_ip, "$@ epdg_swx_hss_ip @$"},
   {dia_swx_remote_port, $@ epdg_swx_hss_port @$},
   {dia_swx_proto, sctp},
   {dia_swx_connect_timer, 30000},
   {dia_swx_watchdog_timer, 30000},
   {dia_swx_watchdog_config,
    [{okay, 3},
     {suspect, 1}]},
   {dia_swx_vendor_id, 0},
   {dia_swx_origin_host, "epdg.localdomain"},
   {dia_swx_origin_realm, "localdomain"},
   {dia_swx_context_id, "epdg@localdomain"},
   % Diameter s6b Connection parameters
   {dia_s6b_local_ip, "$@ epdg_s6b_bind_ip @$"},
   {dia_s6b_local_port, $@ epdg_s6b_bind_port @$},
   {dia_s6b_proto, sctp},
   {dia_s6b_connect_timer, 30000},
   {dia_s6b_watchdog_timer, 30000},
   {dia_s6b_watchdog_config,
   [{okay, 3},
    {suspect, 1}]},
   {dia_s6b_vendor_id, 0},
   {dia_s6b_origin_host, "aaa.localdomain"},
   {dia_s6b_origin_realm, "localdomain"},
   {dia_s6b_context_id, "aaa@localdomain"},
   % GTPv2C Connection parameters
   {gtpc_local_ip, "$@ epdg_gtpc_bind_ip @$"},
   {gtpc_local_port, $@ epdg_gtpc_bind_port @$},
   {gtpc_remote_ip, "$@ epdg_gtpc_pgw_ip @$"},
   {gtpc_remote_port, $@ epdg_gtpc_pgw_port @$}
  ]},
 %% ===========================================
 %% gtp_u_kmod config
 %% ===========================================
  {gtp_u_kmod, [
    %% $@ epdg_tun_interface @$: Iface name, also used to identify the process handling the tunnel.
    {sockets, [{$@ epdg_tun_interface @$, [{create_mode, create},
                       %% ip: IP Address assigned at the tunnel, used as local GTP-U IP Address.
                       {ip, {10,74,0,11}},
                       %% Create the tun socket on a given netns:
                       %%{netns, "mynetns"},
                       %% Bind tun socket to a particular interface (SO_BINDTODEVICE):
                       %%{netdev, "grx"},
                       %% Allow binding to an IP address that is nonlocal or does not (yet) exist (IP_FREEBIND):
                       freebind,
                       % Create gtp tundev with role SGSN:
                       {role, sgsn}
                     ]
              }]
    }
  ]},
 %% ===========================================
 %% gen_socket config
 %% ===========================================
 {gen_socket, [
  {libdir, "_build/default/lib/gen_socket/priv/lib"}
]},

 %% ===========================================
 %% Lager logging config
 %% ===========================================
  {lager, [
    {log_root, "log"},
    {colored, true},
    {handlers,
      [{lager_console_backend, [{level, debug}]},
      {lager_file_backend,
        [{file, "console.log"}, {level, debug}, {size, 104857600}, {date, "$D0"}, {count, 10}]},
      {lager_file_backend,
        [{file, "error.log"}, {level, error}, {size, 104857600}, {date, "$D0"}, {count, 10}]}]},
    {crash_log, "crash.log"},
    {crash_log_msg_size, 65536},
    {crash_log_size, 104857600},
    {crash_log_date, "$D0"},
    {crash_log_count, 10},
    {error_logger_redirect, true}
  ]},
  {kernel, [
   {logger, [{handler, debug, logger_std_h,  % {handler, HandlerId, Module,
              #{config => #{file => "log/erlang.log"}}
            }]
    }
  ]}
].
----
